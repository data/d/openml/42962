# OpenML dataset: oclar

https://www.openml.org/d/42962

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Marwan Al Omari, Moustafa Al-Hajj, Nacereddine Hammami, Amani Sabra
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Opinion+Corpus+for+Lebanese+Arabic+Reviews+%28OCLAR%29) - 2019
**Please cite**: [Paper](https://ieeexplore.ieee.org/document/8716394)

**Opinion Corpus for Lebanese Arabic Reviews (OCLAR) Data Set**

The researchers of OCLAR Marwan et al. (2019), they gathered Arabic costumer reviews from  and Zomato website on wide scope of domain, including restaurants, hotels, hospitals, local shops, etc. The corpus finally contains 3916 reviews in 5-rating scale. For this research purpose, the positive class considers rating stars from 5 to 3 of 3465 reviews, and the negative class is represented from values of 1 and 2 of about 451 texts.

### Attribute Information:

1. 3916 text reviews 
2. 5-rating scale: 1: 303 
2. 148 
3. 418 
4. 734 
5. 2313 

Positive class includes rating stars from 5 to 3 of 3465 total. 
Negative class include rating stars from 1 to 2 of 451 total.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42962) of an [OpenML dataset](https://www.openml.org/d/42962). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42962/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42962/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42962/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

